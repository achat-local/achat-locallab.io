import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => {

  const ville = typeof window !== "undefined" ? window.location.href.split("/")[2].split(".")[0] : "Votre ville"

  return(
    <Layout>
      <SEO title="Home" />
      <p>{ville}</p>
      <h1>Acheter local en ligne, c'est facile ! Bah oui, on est en 2020 ;)</h1>
      <p>Commandez vos livres en ligne et collectez-les chez votre librairie préférée <a href="https://lalibrairie.com">LaLibrairie.com</a></p>
      <p>Achetez chez les commerçant·e·s de votre ville sur <a href="https://fairemescourses.fr">FaireMesCourses.fr</a></p>
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
        <Image />
      </div>
      <Link to="/10-bonnes-raisons-commerce-ethique">10 bonnes raisons d'acheter auprès de commerces éthiques</Link>

      <iframe title="lalibrairie.com" src="https://www.lalibrairie.com/" width="80%" height="400px"></iframe>
      <iframe title="label-emmaus.co" src="https://label-emmaus.co/" width="80%" height="400px"></iframe>
      <iframe title="fairemescourses.fr" src="https://fairemescourses.fr" width="80%" height="400px"></iframe>
      
      
    </Layout>
  )
}

export default IndexPage  
